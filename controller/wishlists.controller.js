// Import du module Express 
const express = require('express');
// Création du gestionnaire de routes modulaires
const router = express.Router();
// Import du module de connexion
const connection = require('../config/db');

//ENDPOINTS

// Route pour afficher toutes les listes de souhaits
router.get("/", (request, response) => {
    connection.query("SELECT * FROM wishlist", (err, results) => {
        if (err) {
            response.status(500).send(err.message)
        } else {
            response.json(results)
        }
    })
});
  
// TEST ENDPOINT GET pour afficher les wishlists par ID wishlists/:wishlist_id
router.get('/:id', (req, res) => {
    // Recupération de l'id passé en paramètre de l'url
    const wishlist_id = req.params.id;
    // Requête SQL pour afficher une wishlist en fonction de son ID
    connection.execute('SELECT * FROM wishlist WHERE wishlist_id = ?',
      [wishlist_id],
      (err, results) => {
        if (err) {
          // Renvoie un message d'erreur
          res.status(500).json(err.message)
        } else {
          // Renvoie un message et le résultat de la requête SQL
          res.status(200).json(results) 
        }
      });
  });

//ENDPOINT GET pour afficher les cadeaux réservés wishlists/:wishlist_id/gifts/:status
router.get('/:wishlist_id/gifts/:status', (req, res) => {
    // Recupération de l'id et du status passés en paramètre de l'url
    const wishlist_id = req.params.wishlist_id;
    // status doit être : true = 1 ou false = 0 
    const status = req.params.status;
    // Requête SQL pour afficher les cadeaux d'une wishlist en fonction de leur status (réservé) true=1/false=0
    connection.execute('SELECT * FROM gift g INNER JOIN wishlist w ON g.wishlist_id = w.wishlist_id WHERE w.wishlist_id = ? AND g.status = ?',
      [wishlist_id, status],
      (err, results) => {
        if (err) {
          // Renvoie un message d'erreur
          res.status(500).json(err.message)
          console.log(results)
        } else {
          // Renvoie un message et le résultat de la requête SQL
          res.status(200).json(results) 
        }
      });
  });

//créer un cadeau depuis une wishlist
router.post("/:id/gifts", (req, res) => {
    const { name, description, link, level, price, status } = req.body;
    const id = req.params.id;
    connection.execute("INSERT INTO gift (name, description, link, level, price, status, wishlist_id) VALUES (?, ?, ?, ?, ?, ?, ?)",
        [name, description, link, level, price, status, id],
        (err, results) => {
            if (err) {
                res.status(500).send(err.message)
            } else {
                const newGift = {
                    ...req.body,
                    gift_id: results.insertId,
                    wishlist_id: id
                }
                res.status(201).json(newGift)
            }
        })
});

// Réserver un cadeau dans une wishlist en étant connecté
router.put('/:id/gifts/:gift_id/users/:user_id', (req, res) => {
    const wishlist_id = req.params.id;
    const gift_id = req.params.gift_id;
    const r_user_id = req.params.user_id;

    connection.execute('SELECT g.name, g.description, g.link, g.level, g.price, g.reservation_user_id FROM gift g INNER JOIN wishlist w ON w.wishlist_id = g.wishlist_id WHERE w.wishlist_id = ? AND g.gift_id = ?',
        [wishlist_id, gift_id],
        (err, results) => {
            if (err) {
                res.send({ error: err.message + "Le cadeau n'a pas été récupéré" })

                // Si le cadeau n'a pas été reservé, reservation_user_id = null, donc on peut lancer la requête
            } else if (results.length === 0) {
                res.status(404).json({ error: `Cadeau ID ${gift_id} de la liste de souhait ID ${wishlist_id} est inexistant` })

            } else if (results.reservation_user_id === null) {

                const giftToChange = results[0];
                const reservedGift = { ...giftToChange, r_user_id };

                connection.execute('UPDATE gift SET reservation_user_id = ?, status = true WHERE gift_id = ?',
                    [r_user_id, gift_id],
                    (err, results) => {
                        if (err) {
                            res.status(500).json({ error: err.message })
                        } else {
                            res.status(200).json(reservedGift)
                        }
                    })
                // Si reservation_user_id != null, c'est que le cadeau a été reservé, on ne lance pas la requête
            } else {
                res.send("Le cadeau a déjà été reservé")
            }
        })
});

//route pour accepter ou non l'invitation à une wishlist
router.put('/:id/users/:received', (req, res) => {
    const user_received = req.params.received //identifiant user qui recoit
    const wishlist_id = req.params.id // identifiant wishlist
    const status = req.body.status// requiert l'entrée du nouveau status

    //condition : si le status refusé -> delete sinon : la requête --> voir la pertinenece plus tard

    // changer l'état de status dans la table invited
    connection.execute('UPDATE invited SET status = ? WHERE wishlist_id = ? && user_id = ?', [status, wishlist_id, user_received], (err, results) => {
        if (err) {
            res.send({ error: err.message })
        } else {
            res.send(`Le status de l'invitation est ${status}`)
        }
    })
})

//ENDPOINT PUT pour modifier un cadeau d'une wishlist /wishlists/:id/gifts/:gift_id
router.put('/:id/gifts/:gift_id', (req, res) => {
    const wishlist_id = req.params.id;
    const gift_id = req.params.gift_id;
    // Récupération des propriétés de l'instance gift_id = ? dans une variable
    const data_gift = req.body;
    //Requête SQL permettant de vérifier l'existance de l'instance gift_id = ?
    connection.execute('SELECT g.name, g.description, g.link, g.level, g.price FROM gift g INNER JOIN wishlist w ON w.wishlist_id = g.wishlist_id WHERE w.wishlist_id = ? AND gift_id = ?',
        [wishlist_id, gift_id],
        (err, results) => {
            if (err) {
                res.send({ error: err.message })
                // Condition vérifiant si l'instance gift_id = ? existe
            } else if (results.length === 0) {
                res.status(404).json({ error: `Cadeau ID ${gift_id} de la liste de souhait ID ${wishlist_id} est inexistant` })
            } else {
                // Récupération du cadeau gift_id = ?
                const giftToUpdate = results[0];
                // Fusion des instances : remplacement des données récupérées (giftToUpdate) par les nouvelles données (data_gift)
                const giftUpdated = { ...giftToUpdate, ...data_gift };
                // Requête SQL pour modifier les champs de gift_id = ? à partir de giftUpdated
                connection.execute('UPDATE gift SET name=?, description=?, link=?, level=?, price=? WHERE gift_id = ?',
                    [giftUpdated.name, giftUpdated.description, giftUpdated.link, giftUpdated.level, giftUpdated.price, gift_id],
                    (err, results) => {
                        if (err) {
                            res.status(500).json({ error: err.message })
                            // Affichage de l'instance modifiée
                        } else {
                            res.status(200).json(giftUpdated)
                        }
                    })
            }
        });
});



//ENDPOINT DELETE pour supprimer une wishlist /wishlist/:id
router.delete('/:id', (req, res) => {
    // Récupération de l'id passée en paramaètre de l'URL dans une constante
    const id = req.params.id;
    // Requête SQL permettant de supprimer une wishlist spécifique en fonction de son ID
    connection.execute('DELETE FROM wishlist WHERE wishlist_id = ?', [id], (err, results) => {
      if (err) {
        // Renvoi d'un message d'erreur
        res.status(500).json(err.message)
      } else {
        //Renvoi d'un message de confirmation avec l'ID de la wishlist supprimée
        res.json(`Wishlist #ID ${id} a bien été supprimé.`)
      }
    })
  });

//route pour supprimer un cadeau d'une wishlist
router.delete('/:id/gifts/:gift_id', (req, res) => {
    const wishlist_id = req.params.id // identifiant wishlist
    const gift_id = req.params.gift_id//identifiant cadeau

    //récupère les propriétés de gift et le nom de la wishlist
    connection.execute('SELECT g.name gift_name, g.description, g.link, g.level, g.price, w.name wishlist_name FROM gift g INNER JOIN wishlist w ON w.wishlist_id = g.wishlist_id WHERE w.wishlist_id = ? AND gift_id = ?', [wishlist_id, gift_id], (err, results) => {
        if (err) {
            res.send({ error: err.message })
            // Condition vérifiant si l'instance gift_id = ? existe
        } else if (results.length === 0) {
            res.status(404).json({ error: `Cadeau ID ${gift_id} de la liste de souhait ID ${wishlist_id} est inexistant` })
        } else {
            //si gift_id existe je récupère le nom du cadeau et le stocke dans giftName
            // console.log(results)
            const giftName = results[0].gift_name
            //si wishlist_id existe je récupère le nom de la wishlist et le stocke dans wishlistName
            const wishlistName = results[0].wishlist_name

            //supprime le cadeau de la wishlist
            connection.execute('DELETE FROM gift WHERE gift_id = ?', [gift_id], (deleteErr, deleteResults) => {
                if (deleteErr) {
                    res.send({ error: deleteErr.message })
                } else {
                    res.send(`Le cadeau ${giftName} a été supprimé de ${wishlistName}`)
                }
            })
        }
    })
})

module.exports = router;