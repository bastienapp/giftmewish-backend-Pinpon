// Import du module Express 
const express = require('express');
// Création du gestionnaire de routes modulaires
const router = express.Router();
// Import du module de connexion
const connection = require('../config/db');
// Import bcrypt
const bcrypt = require('bcrypt')
// Import du middleware validator.password
const { validatePassword } = require('../middleware/validatorPassword.middleware.js')

// afficher tous les users
router.get("/", (request, response) => {
  connection.query("SELECT * FROM user", (err, results) => {
    if (err) {
      response.status(500).send(err.message)
    } else {
      response.json(results)
    }
  })
});


// route pour créer une liste de souhaits
router.post("/:id/wishlists", (request, response) => {

  const id = request.params.id; //variable id qui contient ce qui est attendu en paramètre dans l'URL

  const { name, theme, description, event_date } = request.body; //ce qui sera contenu dans le corps de ma requête

  connection.execute('INSERT INTO wishlist(name, theme, description, event_date, user_id) VALUES (?, ?, ?, ?, ?)', [name, theme, description, event_date, id], (err, results) => {

    if (err) {
      //message d'erreur si un problème survient au niveau du serveur 
      response.status(500).send('requête mal effectuée');
    } else {
      response.json(
        //affiche les propriétés du nouvel objet créé avec leurs valeurs
        { ...request.body, wishlist_id: results.insertId });
    }
  })
})

  // /:user_send/wishlists/:id/users/:received
  //inviter des utilisateurs à des wishlists
router.post('/:user_send/wishlists/:id', (req, res) => {
    const wishlist_id = req.params.id // identifiant wishlist
    // const user_send = req.params.send//identifiant user qui envoie
    // const user_received = req.params.received//identifiant user qui recoit
    // req.body pour envoyer la demande en fonction du pseudo
    const pseudo = req.body.pseudo
  
    //récupère les données d'une wishlist
    connection.execute('SELECT * FROM wishlist WHERE wishlist_id = ?', [wishlist_id], (err, results) => {
      if (err) {
        res.send({ error: err.message })
      } else if (results.length === 0) {
        res.send('pas de Wishlist à cet identifiant')
      } else {
        connection.execute('SELECT * FROM user WHERE pseudo LIKE ?', [pseudo], (err, results) => { // recherche de l'utilisateur en fonction du pseudo exacte
          if (err) {
            res.send({ error: err.message })
          } else if (results.length === 0){
            res.send(`Pas d'utilisateur pour le pseudo ${pseudo}`)
          }else{
            const user_received = results[0].user_id
            //envoie de l'invitation
            connection.execute('INSERT INTO invited VALUES (?,?,?)', [user_received, wishlist_id, 'pending'], (err, results) => {
              if (err) {
                res.send({ error: err.message })
              } else {
                res.send(`invitation envoyée à ${pseudo} dont l'id est ${user_received}`)
              }
            })
          }
        })
      }
    })
  })


//ENDPOINT GET pour afficher les wishlists qui ont été partagées à un utilisateur donné /users/:id/wishlists/:status
router.get('/:id/wishlists/:status', (req, res) => {
  // Récupération de l'id de l'user passé en paramètre
  const user_id = req.params.id;
  // Récupération du status de l'invitation passé en paramètre
  const status = req.params.status;
  // Récupération des noms des wishlists pour un utilisateur spécifique et pour un status spécifique
  connection.execute('SELECT u.pseudo, w.name FROM user u INNER JOIN invited i ON u.user_id = i.user_id INNER JOIN wishlist w ON w.wishlist_id = i.wishlist_id WHERE u.user_id = ? AND status = ?',
    [user_id, status],
    (err, results) => {
      if (err) {
        res.status(500).json({ error: err.message })
      } else if (results.length === 0) {
        res.status(200).json(`Aucune liste de souhaite trouvée ayant le status ${status} pour l'utilisateur ${user_id}`)
      } else {
        // Récupération des noms des listes de souhaits grâce à un .map
        const shared_wishlist = results.map(wishlist => wishlist.name)
        // Affichage d'un message contenant les données récupérées en fonction du status 
        if (status == 'accepted') {
          res.status(200).json(`L'utilisateur ${results[0].pseudo} est invité sur les listes de souhaits suivantes : ${shared_wishlist}`)
        } else if (status === 'pending') {
          res.status(200).json(`Les listes de souhaits suivantes : ${shared_wishlist} sont en attente de validation par ${results[0].pseudo}`)
        } else {
          res.status(200).json(`Les listes de souhaits suivantes : ${shared_wishlist} ont été annulés par ${results[0].pseudo}`)
        }
      }
    });
});

// ENDPOINT PUT qui vérifie que le mot de passe et l'email correspondent pour pouvoir changer le mot de passe
router.put('/:id', validatePassword, async (req, res) => {
  const id = req.params.id;
  // Permet de saisir le mdp actuel à comparer et le nouveau mdp
  const { pseudo, email, password, newPassword } = req.body;
  // Hashe le mdp avec bcrypt
  const hashedPassword = await bcrypt.hash(newPassword, 13)

  // Vérifie que le pseudo et l'email correspondent
  connection.execute('SELECT * FROM user WHERE user_id = ? AND pseudo = ? AND email = ?',
    [id, pseudo, email],
    async (err, results) => {
      // console.log(password, results[0].password)
      if (err) {
        res.status(500).json({ error: err.message })
        // Vérifie l'existence de l'utilisateur par son ID
      } else if (results.length === 0) {
        res.status(404).json(`L'utilisateur ID ${id} n'existe pas`)
        // Compare le mdp saisi dans le body avec le mdp actuel de l'utilisateur
      } else if (await bcrypt.compare(password, results[0].password,)) {
        // TODO MIDDLEWARE
        if (newPassword.length < 8) {
          res.status(200).json('Mot de passe trop court')
        } else {
          // Supprime les mots de passe en clair
          delete req.body.password
          delete req.body.newPassword
          const userToUpdate = results[0]
          const UserUpdated = { ...userToUpdate, ...req.body }
          // Modifie le mdp dans la base de données si toutes les conditions on étaient remplies
          connection.execute('UPDATE user SET password = ? WHERE pseudo = ? AND email = ? AND user_id = ?', [hashedPassword, pseudo, email, id], (err, results) => {
            if (err) {
              res.status(500).json({ error: err.message })
            } else {
              res.status(200).json({ UserUpdated })
            }
          })
        }
      } else {
        res.status(500).json('Erreur')
      }
    })
});

//Route pour afficher ses informations en temps qu'utilisateur connecté
// A Améliorer, pour l'instant n'importe qui peut afficher les informations avec l'id de l'utilisateur
router.get('/:id', (req, res) => {
  const user_id = req.params.id;

  connection.execute('SELECT pseudo, email FROM user WHERE user_id = ?',
    [user_id],
    (err, results) => {
      if (err) {
        res.status(500).json({ error: err.message })
      } else {
        res.status(200).json(results)
      }
    })
})

module.exports = router;