// Import du module Express 
const express = require('express');
// Création du gestionnaire de routes modulaires
const router = express.Router();
// Import du module de connexion
const connection = require('../config/db');

// Route pour afficher tous les cadeaux
router.get("/", (request, response) => {
    connection.query("SELECT * FROM gift", (err, results) => {
      if (err) {
        response.status(500).send(err.message)
      } else {
        response.json(results)
      }
    })
  });

module.exports = router;