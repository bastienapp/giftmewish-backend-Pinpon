const validateRegister = (req, res, next) => {
    //vérifie que les champs attendus sont bien remplis
    const { pseudo, email, password } = req.body

    if(!pseudo || !email || !password){
        return res.status(400).json({ message: 'L\'email, le pseudo, le mot de passe sont des champs obligatoires.'})
    }

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    //condition pour vérifier si l'email est correcte
    if(!email.match(re)){
        return res.status(400).json({ message: 'Ce n\'est pas une adresse mail.'})
    }

    if(password.length < 8) {
        return res.status(400).json({ message: 'Le mot de passe doit contenir au moins 8 caractères'})
    }


    next()
  }
  
  module.exports = { validateRegister }
