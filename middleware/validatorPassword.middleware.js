const validatePassword = (req, res, next) => {
    const { newPassword } = req.body;

    if (newPassword.length < 8) {
        return res.status(200).json("Le mot de passe doit contenir au minimum 8 caractères")
    }

    next()
};

module.exports = { validatePassword };