const validateLogin = (req, res, next) => {
    //vérifie que les champs attendus sont bien remplis
    const { email, password } = req.body

    if(!email || !password){
        return res.status(400).json({ message: 'L\'email et le mot de passe sont des champs obligatoires.'})
    }

    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    //condition pour vérifier si l'email est correcte
    if(!email.match(re)){
        return res.status(400).json({ message: 'Ce n\'est pas une adresse mail.'})
    }

    next()
  }
  
  module.exports = { validateLogin }
