const express = require("express")
const app = express()
const { validateLogin } = require('./validator.middleware')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const connection = require("./config/db");

// Import du controller wishlists
const wishlistsController = require('./controller/wishlists.controller');
// Import du controller gifts
const giftsController = require('./controller/gifts.controller')
//Import du controller users
const usersController = require('./controller/users.controller');

const { validateLogin } = require('./middleware/validatorLogin.middleware')
const { validateRegister } = require("./middleware/validatorRegister.middleware");

// Utilisation du controller pour gérer les routes de wishlists
app.use('/wishlists', wishlistsController);
// Utilisation du controller pour gérer les routes de gifts
app.use('/gifts', giftsController);
// Utilisation du controller pour gérer les routes de users
app.use('/users', usersController);


//route pour se connecter (LOGIN)
app.post('/login', validateLogin, async (req, res) => {
  const { email, password } = req.body //éléments recquis dans le corps de la requête

  connection.execute('SELECT * FROM user WHERE email = ?', [email], async (err, results) => { // récupère toutes les informations rattachées au user qui correspond à l'adresse email
    if (err) {
      res.status(500).json({ message: err.message })
    } else {
      if (results.length > 0) {

        const user = results[0] // stocke dans la variable user toutes les informations récupérées correpondantes à l'email saisi

        // Comparaison du mot de passe haché avec celui stocké en base de données
        const isPasswordValid = await bcrypt.compare(password, user.password)

        if (isPasswordValid) {
          const token = jwt.sign({user_id : user.user_id}, 'your-secret-key', {expiresIn: '00h02'})
          res.status(200).json({ message: `Connexion établie : ${token}` }) // la comparaison match
        } else {
          res.status(401).json({ message: 'Email ou mot de passe incorrect' }) // la comparaison un mdp ne match pas
        }
      } else {
        res.status(401).json({ message: 'Email ou mot de passe incorrect' }) // l'email n'est pas bon, mais pour pas mettre sur la piste un message d'erreur large, dans le doute où ce message d'erreur soit accessible par le navigateur
      }
    }
  })
})

// ENDPOINT GET pour afficher tous les utilisateurs
app.get('/users', (req, res) => {
  connection.execute('SELECT * FROM user', (err, results) => {
    if (err) {
      res.status(500).json({ error: err.message })
    } else {
      res.status(200).json(results)
    }
  })
})


// Route pour créer un nouvel utilisateur
app.post('/register', validateRegister, async (req, res) => {
  const { pseudo, email, password } = req.body;
  const hash = await bcrypt.hash(password, 13)

  connection.execute('INSERT INTO user (pseudo, email, password) VALUES (?, ?, ?)',
    [pseudo, email, hash],
    (err, results) => {
      if (err) {
        res.status(500).json({ error: err.message })
      } else {
        delete req.body.password
        {
          const newUser = {
            ...req.body,
            user_id: results.insertId
          };
          res.status(201).json(newUser)
        }
      }
    })
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server launched on http://localhost:${port}`);
})
